package iisis.trade.movex.message;

import iisis.trade.movex.storage.Storage;
import iisis.trade.movex.telegram.TelegramService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.Date;
import java.util.HashMap;

@Slf4j
@Service
public class MessageService {

    @Autowired
    TelegramService telegramService;

    @Autowired
    Storage storage;

    @Value("${iisis.trade.movex.message.MIN_MESSAGE_PERIOD_SEC}")
    private Integer MIN_MESSAGE_PERIOD_SEC;

    HashMap<String, Long> sendMessageList = new HashMap<>();

    public void sendMessage(Long userId, HashMap<String, String> messageData) {

        final Long currentTime = System.currentTimeMillis();
//        final String sendMessageListKey = messageData.get("pair") + "." + messageData.get("timeFrame");
        final String sendMessageListKey = messageData.get("pair");

        if (this.sendMessageList.containsKey(sendMessageListKey)) {

            var lastMessageTime = this.sendMessageList.get(sendMessageListKey);

            if ((lastMessageTime + this.MIN_MESSAGE_PERIOD_SEC * 1000) > currentTime) {
                log.warn("Message is blocked by MIN_MESSAGE_PERIOD_SEC: {}. Last time(sec):  {}, {}", MIN_MESSAGE_PERIOD_SEC, currentTime - lastMessageTime, messageData);
                return;
            }
        }

        final String message = this.messageBuilder(messageData);
        final String userChatId = storage.getChatIdByUserId(userId).get();
        telegramService.sendMessage(userChatId, message);
        this.sendMessageList.put(sendMessageListKey, currentTime);

    }

    public String messageBuilder(HashMap<String, String> messageData) {

        LocalDate localDate = LocalDate.now();
        Date date = new Date();
        String dateFormatted = localDate + " " + date.toString().substring(11, 16);

        StringBuilder sb = new StringBuilder();
        Float priceDifferenceFloat = Float.parseFloat(messageData.get("priceDifference"));
        Float priceDifferenceFloatRound = (float) Math.round(priceDifferenceFloat * 100);
        Float priceDifferenceRound = priceDifferenceFloatRound / 100;

        sb.append("<strong>" + messageData.get("pair") + "</strong>")

                .append(" (" + messageData.get("timeFrame") + ")" + " - ")
                .append(messageData.get("tradePrice") + " / ")
                .append("<strong>" + priceDifferenceRound + "</strong>" + " / ")
                .append(messageData.get("percentPriceLimit") + "\n")


                .append("<strong>FirstBar: </strong>\n")
//                .append("date: " + dateFormatted + ", \n")
                .append("open: " + messageData.get("firstBarOpenPrice") + ", \n")
                .append("close: " + messageData.get("firstBarClosePrice") + ", \n")
                .append("low: " + messageData.get("firstBarLowPrice") + ", \n")
                .append("high: " + messageData.get("firstBarHighPrice") + "\n")
                .append("<strong>LastBar: </strong>\n")
//                .append("date: " + dateFormatted + ", \n")
                .append("open: " + messageData.get("lastBarOpenPrice") + ", \n")
                .append("close: " + messageData.get("lastBarClosePrice") + ", \n")
                .append("low: " + messageData.get("lastBarLowPrice") + ", \n")
                .append("high: " + messageData.get("lastBarHighPrice") + "\n");

        return sb.toString();

    }

}
