package iisis.trade.movex.storage;

import iisis.trade.movex.pair.PairAdapter;
import iisis.trade.movex.userPairSettings.UserPairSettingsService;
import iisis.trade.movex.userPairSettings.dto.UserPairSettings;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;

import java.util.*;

@Slf4j
@Getter
@Setter
@Controller
/**
 * Класс выполняет кеширование информации из БД и является билдером для создания PairAdapter
 */
public class Storage {

    @Autowired
    UserPairSettingsService userPairSettingsService;

    private final HashMap<String, List<UserPairSettings>> userPairSettingsMap = new HashMap<>(); //pair
    private final HashMap<String, PairAdapter> pairAdapterMap = new HashMap<>();
    private final HashMap<Long, String> telegramChatIdsMap = new HashMap();

    public List<PairAdapter> getPairAdapterList(String pair) {

        var settings = userPairSettingsMap.get(pair);

        if (settings == null) {
            settings = userPairSettingsService.getUserPairSettingsListByPair(pair);

            userPairSettingsMap.put(pair, settings);

            Set<Long> ids = new HashSet();
            for (UserPairSettings setting : settings) {
                ids.add(setting.getUserId());
            }

            List<UserPairSettings> finalSettings = settings;
            ids.stream().forEach(userId -> {

                List<UserPairSettings> filteredSettings = finalSettings.stream().filter(i -> i.getUserId().equals(userId)).toList();


                final var frameAndLimitsMap = new HashMap<Integer, Float>();

                for (UserPairSettings el : filteredSettings) {
                    frameAndLimitsMap.put(el.getTimeFrame(), el.getPercentPriceLimit());
                }

                final PairAdapter adapter = new PairAdapter(userId, pair, frameAndLimitsMap);
                this.pairAdapterMap.put(userId + "." + pair, adapter);
            });

        }

        final List<PairAdapter> result = new ArrayList<>();
        settings.forEach(v -> {

            var el = pairAdapterMap.get(v.getUserId() + "." + pair);
            if (el != null) {
                result.add(el);
            } else {
                //TODO ТСН как обрабатывать, если не содержит значение. Возможно ошибка или произошло обновление. Попытаться вызвать экспешн?
            }
        });

        return result;
    }


    public Optional<String> getChatIdByUserId(Long userId) {

        if (telegramChatIdsMap.containsKey(userId)) {
            return Optional.of(telegramChatIdsMap.get(userId));
        }

        for (Map.Entry<String, List<UserPairSettings>> v : userPairSettingsMap.entrySet()) {
            for (UserPairSettings s : v.getValue()) {
                if (s.getUserId().equals(userId)) {
                    telegramChatIdsMap.put(s.getUserId(), s.getUserChatId());
                    return Optional.of(s.getUserChatId());
                }
            }
        }
        return Optional.empty();
    }

}
