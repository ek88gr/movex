package iisis.trade.movex;

import iisis.trade.movex.externalSource.binance.BinanceService;
import iisis.trade.movex.externalSource.binance.webSocketClient.WebSocketClient;
import iisis.trade.movex.externalSource.tinkoff.TinkoffService;
import jakarta.annotation.PostConstruct;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@Slf4j
@SpringBootApplication
public class Main {

    @Autowired
    BinanceService binanceService;

    @Autowired
    TinkoffService tinkoffService;

    @Autowired
    WebSocketClient webSocketClient;

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
    }

    @PostConstruct
    private void post() {

		binanceService.init();
		tinkoffService.init();
    }

}
