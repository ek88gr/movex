package iisis.trade.movex.telegram;

import lombok.extern.slf4j.Slf4j;
import org.telegram.telegrambots.bots.TelegramLongPollingBot;
import org.telegram.telegrambots.meta.api.methods.send.SendMessage;
import org.telegram.telegrambots.meta.api.objects.Update;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;

@Slf4j
public class Bot extends TelegramLongPollingBot {

    private final String name;
    private final String token;

    public Bot(String name, String token) {
        this.name = name;
        this.token = token;
    }

    @Override
    public String getBotUsername() {
        return name; // "parsebarbot"
    }

    @Override
    public String getBotToken() {
        return token; // "5372209451:AAFcgHOxYkvRTt4R2pWisBVFFa0oTqnlmvU";
    }

    @Override
    public void onUpdateReceived(Update update) {
        log.info("Telegram chat_id: {}", update.getMessage().getChatId());
    }


    public void sendMessage(String chatId, String text) {

        SendMessage answer = new SendMessage();
        answer.enableHtml(true);
        answer.disableWebPagePreview();
        answer.setText(text);
        answer.setChatId(chatId);
        try {
            execute(answer);
        } catch (TelegramApiException e) {
            log.error("ERROR: {}", e);
        }
    }

}