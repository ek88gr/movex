package iisis.trade.movex.telegram;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

@Slf4j
@Service
public class TelegramService {

//    @Value("${iisis.trade.movex.telegram.BOT_NAME}")
    final private String BOT_NAME;
//    @Value("${iisis.trade.movex.telegram.BOT_TOKEN}")
    final private String BOT_TOKEN;

    private Bot bot;

    public TelegramService(
            @Value("${iisis.trade.movex.telegram.BOT_NAME}") String BOT_NAME,
            @Value("${iisis.trade.movex.telegram.BOT_TOKEN}") String BOT_TOKEN) throws TelegramApiException {

        this.BOT_NAME = BOT_NAME;
        this.BOT_TOKEN = BOT_TOKEN;

        this.bot = new Bot(BOT_NAME, BOT_TOKEN);

        TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
        botsApi.registerBot(this.bot);
    }

    public void sendMessage(String userChatId, String message) {
//        this.bot.sendMessage(this.CHAT_ID, message);
//        this.bot.sendMessage("-750522799", message);
        this.bot.sendMessage(userChatId, message);
    }

}
