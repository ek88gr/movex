package iisis.trade.movex.pair;

import iisis.trade.movex.message.MessageService;
import iisis.trade.movex.pair.models.MarketTrade;
import iisis.trade.movex.pair.models.PriceDifference;
import iisis.trade.movex.storage.Storage;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.ta4j.core.Bar;
import org.ta4j.core.num.DoubleNum;
import org.ta4j.core.num.Num;

import java.time.*;
import java.util.*;

@Slf4j
@Getter
@Setter
@Service
public class PairService {

    @Autowired
    Storage storage;

    @Autowired
    MessageService messageService;


    public void putStream(List<MarketTrade> tradeList) {

        for (MarketTrade trade : tradeList) {

            final String pair = trade.getCurrencyPair().toString();
            final List<PairAdapter> adapters = storage.getPairAdapterList(pair);

            if (adapters.isEmpty()) return;

            adapters.forEach(v -> this.updatePairAdapters(v, trade));

        }

    }


    private void updatePairAdapters(PairAdapter pairAdapter, MarketTrade trade) {

        // 1. добавляется новый бар с одинаковыми значениями low и high
        // 2. добавляется новый трейд в страный бар, но показатели low и high не обновляются
        // 3. добавляется новый трейд в старый бар и обновляются показатели

        final var frameLimitsMap = pairAdapter.getFrameLimits();

        for (Map.Entry<Integer, Float> item : frameLimitsMap.entrySet()) {
            final Num tradePrice = DoubleNum.valueOf(trade.getPrice());
            final ZonedDateTime tradeTime = Instant.ofEpochMilli(trade.getTradeTime()).atZone(ZoneId.systemDefault());

            final Optional<Bar> data = pairAdapter.addDataBar(item.getKey(), tradeTime, tradePrice, trade.getQuantity());
            if (data.isEmpty()) {
                continue;
            }

            final Bar lastBar = data.get();

            if (this.priceNotEqualLowOrHigh(tradePrice, lastBar.getLowPrice(), lastBar.getHighPrice())) {
                continue;
            }

            final Optional<PriceDifference> optionalPriceDifference = pairAdapter.comparePriceBar(item.getKey());


            if (optionalPriceDifference.isEmpty()) {
                continue;
            }

            final PriceDifference priceDifference = optionalPriceDifference.get();

             if(Float.compare(item.getValue(), priceDifference.getPriceDifference()) > 0){
                 continue;
             }


            final Bar firstBar = priceDifference.getFirstBar();

            HashMap<String, String> messageData = new HashMap<>();
            messageData.put("pair", String.valueOf(trade.getCurrencyPair()));
            messageData.put("tradePrice", String.valueOf(trade.getPrice()));
            messageData.put("priceDifference", String.valueOf(optionalPriceDifference.get().getPriceDifference()));
            messageData.put("percentPriceLimit", String.valueOf(item.getValue()));
            messageData.put("timeFrame", String.valueOf(item.getKey()));

            messageData.put("firstBarOpenPrice", String.valueOf(firstBar.getOpenPrice()));
            messageData.put("firstBarClosePrice", String.valueOf(firstBar.getClosePrice()));
            messageData.put("firstBarLowPrice", String.valueOf(firstBar.getLowPrice()));
            messageData.put("firstBarHighPrice", String.valueOf(firstBar.getHighPrice()));

            messageData.put("lastBarOpenPrice", String.valueOf(lastBar.getOpenPrice()));
            messageData.put("lastBarClosePrice", String.valueOf(lastBar.getClosePrice()));
            messageData.put("lastBarLowPrice", String.valueOf(lastBar.getLowPrice()));
            messageData.put("lastBarHighPrice", String.valueOf(lastBar.getHighPrice()));



            messageService.sendMessage(pairAdapter.getUserId(), messageData);

        }

    }


    public boolean priceNotEqualLowOrHigh(Num price, Num low, Num high) {
        final boolean result = price.isEqual(low) || price.isEqual(high);
        return !result;
    }

}
