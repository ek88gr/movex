package iisis.trade.movex.pair.models;

import lombok.AllArgsConstructor;
import lombok.Getter;
import org.ta4j.core.Bar;

@Getter
@AllArgsConstructor
public class PriceDifference {
    final Bar firstBar;
    final Bar lastBar;
    final int priceDirection;
    final Float priceDifference;


//    public PriceDifference(Bar firstBar, Bar lastBar, Float priceDifference, int priceDirection) {
//        this.firstBar = firstBar;
//        this.lastBar = lastBar;
//        this.priceDirection = priceDirection;
//        this.priceDifference = priceDifference;
//    }
}
