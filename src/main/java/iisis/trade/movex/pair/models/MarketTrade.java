package iisis.trade.movex.pair.models;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.currency.CurrencyPair;

@Slf4j
@Getter
@Setter
public class MarketTrade {

    private String eventType;

    private long eventTime;

    private CurrencyPair currencyPair;

    private long tradeTime;

    private double quantity;

    private double price;

}
