package iisis.trade.movex.pair;

import iisis.trade.movex.pair.models.PriceDifference;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.ta4j.core.Bar;
import org.ta4j.core.BaseBar;
import org.ta4j.core.BaseBarSeries;
import org.ta4j.core.num.DoubleNum;
import org.ta4j.core.num.Num;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashMap;
import java.util.Map;
import java.util.Optional;

@Slf4j
@Getter
@Setter
public class PairAdapter {

    private String pair;
    private Long userId;
    private HashMap<Integer, BaseBarSeries> map = new HashMap<>();
    private final Duration BASE_BAR_DURATION = Duration.ofMinutes(1);
    private final Map<Integer, Float> limits;

    public PairAdapter(Long userId, String pair, Map<Integer, Float> limits) {
        this.userId = userId;
        this.pair = pair;
        this.limits = limits;

        limits.forEach((k, v) -> {
            BaseBarSeries series = new BaseBarSeries(v.toString(), DoubleNum::valueOf);
            series.setMaximumBarCount(k);
            map.put(k, series);
        });
    }

    public Optional<Bar> addDataBar(Integer timeFrame, ZonedDateTime timestamp, Num tradePrice, Double volume) {
//            final ZonedDateTime zonedDateTime = Instant.ofEpochMilli(unixTime).atZone(ZoneId.systemDefault());
        final Num numVolume = DoubleNum.valueOf(volume);

        final BaseBarSeries series = this.getSeriesByTimeFrame(timeFrame);
        final Bar result;

        if (series.getBarCount() == 0) {
            result = new BaseBar(BASE_BAR_DURATION, timestamp.plus(BASE_BAR_DURATION), DoubleNum::valueOf);
            result.addTrade(numVolume, tradePrice);
            series.addBar(result);
        } else {
            final Optional<Bar> data = this.getLastBar(series, timestamp);
            if (data.isEmpty()) {
                return Optional.empty();
            }
            result = data.get();
            result.addTrade(numVolume, tradePrice);
        }

        return Optional.of(result);
    }

    public BaseBarSeries getSeriesByTimeFrame(Integer timeFrame) {
        if (map.containsKey(timeFrame)) return map.get(timeFrame);
        throw new RuntimeException(new RuntimeException().getMessage());
    }

    public Optional<PriceDifference> comparePriceBar(Integer timeFrame, BaseBarSeries series) {

        if (timeFrame > series.getBarCount()) return Optional.empty();

        final Bar fBar = series.getFirstBar();
        final Num fBarLow = fBar.getLowPrice();
        final Num fBarHigh = fBar.getHighPrice();

        final Bar lBar = series.getLastBar();
        final Num lBarClose = lBar.getClosePrice();

        //не можем определить направление движения цены: Bearish or Bullish
        if(lBarClose.isGreaterThanOrEqual(fBarLow) && lBarClose.isLessThanOrEqual(fBarHigh)) return Optional.empty();

        final int priceDirection = fBarLow.compareTo(lBarClose);

        final Num priceDifference;
        if (priceDirection > 0) {
            //Bearish
            final Num lBarLow = lBar.getLowPrice();
            priceDifference = fBarHigh.minus(lBarLow).dividedBy(lBarLow).multipliedBy(series.numOf(100));
        } else {
            //Bullish
            final Num lBarHigh = lBar.getHighPrice();
            priceDifference = lBarHigh.minus(fBarLow).dividedBy(fBarLow).multipliedBy(series.numOf(100));
        }

        return Optional.of(new PriceDifference(fBar, lBar, priceDirection, priceDifference.floatValue()));
    }

    /**
     * Была проблема, что на примере нисходящего тренда priceDifference заметно снижался по сравнению со восходящим или
     * уходил в минус, хотя значения должны быть одинаковыми (зеркальное отражение). Добавил условие, поправил формулу.
     */
    public Optional<PriceDifference> comparePriceBar(Integer timeFrame) {
        final BaseBarSeries series = this.getSeriesByTimeFrame(timeFrame);
        return this.comparePriceBar(timeFrame, series);
    }

    public Map<Integer, Float> getFrameLimits() {
        return this.limits;
    }

    /**
     * Дозаполняет бары в периодах, где не было трейдов, устанавливая в каждый
     * из них дефолтную цену из цены закрытия последнего бара, где был трейд.
     */
    private Optional<Bar> getLastBar(BaseBarSeries series, ZonedDateTime timestamp) {

        Bar lastBar = series.getLastBar(); //последний бар
        final Num lastBarClosePrice = lastBar.getClosePrice(); //цена закрытия последнего бара
        ZonedDateTime lastBarBeginTime = lastBar.getBeginTime(); //время начала последнего бара
        ZonedDateTime lastBarEndTime = lastBar.getEndTime(); //время окончания последнего бара

        if (timestamp.isBefore(lastBarBeginTime)) { //время начала последнего бара
            log.error("TradeTime < BeginTime of LastBar. TradeTime : {} / lastBar.getBeginTime(): {}", timestamp, lastBar.getBeginTime());
            return Optional.empty();
        }


        while (!timestamp.isBefore(lastBarEndTime)) {
//        timestamp.equals(lastBarEndTime) || timestamp.isAfter(lastBarEndTime)
            if (lastBar.getClosePrice() == null) {
                lastBar.addPrice(lastBarClosePrice);
            }

            lastBar = new BaseBar(BASE_BAR_DURATION, lastBarEndTime.plus(BASE_BAR_DURATION), DoubleNum::valueOf);
            lastBarEndTime = lastBar.getEndTime();

            series.addBar(lastBar);

        }

        return Optional.of(lastBar);
    }

}




