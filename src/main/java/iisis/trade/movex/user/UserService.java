package iisis.trade.movex.user;

import iisis.trade.movex.user.dao.UserRepository;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

@Slf4j
@Getter
@Setter
@Service
public class UserService {

    @Autowired
    UserRepository userRepository;

}
