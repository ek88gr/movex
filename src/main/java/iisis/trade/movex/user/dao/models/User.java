package iisis.trade.movex.user.dao.models;

import iisis.trade.movex.userPairSettings.dao.models.UserPairSettingsListRowModel;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.util.List;

@Slf4j
@Getter
@Setter
@Entity
@Table(name = "users", indexes = { @Index(name = "k_chat_element_chat_id", columnList = "chat_id") })
public class User {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(nullable = false, name = "id")
    private Long id;

    @Column(nullable = false, name = "chat_id")
    private String chatId;

    @OneToMany(cascade = CascadeType.ALL, fetch = FetchType.EAGER, mappedBy = "user")
    private List<UserPairSettingsListRowModel> userSettingsList;

}
