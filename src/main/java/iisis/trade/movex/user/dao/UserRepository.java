package iisis.trade.movex.user.dao;

import iisis.trade.movex.user.dao.models.User;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserRepository extends CrudRepository<User, Long> {
    List<User> findUsersById(Long id);
}
