package iisis.trade.movex.userPairSettings.dto;

import lombok.extern.slf4j.Slf4j;
import lombok.Getter;
import lombok.AllArgsConstructor;

@Slf4j
@Getter
@AllArgsConstructor
//прокси-объект, без сеттеров
public class UserPairSettings {
    private Long userId;
    private String userChatId;
    private String pair;
    private Integer timeFrame;
    private Float percentPriceLimit;
}
