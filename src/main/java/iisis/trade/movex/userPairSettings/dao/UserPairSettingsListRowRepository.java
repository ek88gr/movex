package iisis.trade.movex.userPairSettings.dao;

import iisis.trade.movex.userPairSettings.dao.models.UserPairSettingsListRowModel;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface UserPairSettingsListRowRepository extends CrudRepository<UserPairSettingsListRowModel, Long> {
    List<UserPairSettingsListRowModel> findByPair(String pair);
}
