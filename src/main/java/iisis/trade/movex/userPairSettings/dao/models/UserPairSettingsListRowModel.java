package iisis.trade.movex.userPairSettings.dao.models;

import iisis.trade.movex.user.dao.models.User;
import jakarta.persistence.*;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "pair_element", indexes = { @Index(name = "k_pair_element_pair", columnList = "pair") })
public class UserPairSettingsListRowModel {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column
    private Long id;

    @Column(nullable = false, name = "pair")
    String pair;

    @Column(nullable = false, name = "time_frame")
    Integer timeFrame;

    @Column(nullable = false, name = "percent_price_limit")
    Float percentPriceLimit;

    @ManyToOne(cascade = CascadeType.ALL, fetch = FetchType.EAGER)
    @JoinColumn(name = "user_id")
    private User user;

}
