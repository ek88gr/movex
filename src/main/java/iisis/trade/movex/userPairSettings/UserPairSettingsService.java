package iisis.trade.movex.userPairSettings;

import iisis.trade.movex.telegram.TelegramService;
import iisis.trade.movex.user.UserService;
import iisis.trade.movex.user.dao.models.User;
import iisis.trade.movex.userPairSettings.dao.UserPairSettingsListRowRepository;
import iisis.trade.movex.userPairSettings.dao.models.UserPairSettingsListRowModel;
import iisis.trade.movex.userPairSettings.dto.UserPairSettings;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
@Getter
@Setter
@Slf4j
public class UserPairSettingsService {

    @Autowired
    UserPairSettingsListRowRepository repository;


    public void save(String pair, Integer timeFrame, Float percentPriceLimit) {
        final UserPairSettingsListRowModel item = new UserPairSettingsListRowModel();
        item.setPair(pair);
        item.setTimeFrame(timeFrame);
        item.setPercentPriceLimit(percentPriceLimit);
        repository.save(item);
    }


    public List<UserPairSettings> getUserPairSettingsListByPair(String pair) {
        final var userPairSettingsList = new ArrayList<UserPairSettings>();

            repository.findByPair(pair).forEach(v -> {
                User user = v.getUser();
                UserPairSettings userPairSettings = new UserPairSettings(
                        user.getId(),
                        user.getChatId(),
                        v.getPair(),
                        v.getTimeFrame(),
                        v.getPercentPriceLimit()
                );
                userPairSettingsList.add(userPairSettings);
            });

        return userPairSettingsList;
    }

}
