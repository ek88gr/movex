package iisis.trade.movex.externalSource.tinkoff.api;

import io.smallrye.mutiny.Multi;
import lombok.extern.slf4j.Slf4j;
import org.reactivestreams.FlowAdapters;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.*;
import ru.tinkoff.piapi.core.InvestApi;
import ru.tinkoff.piapi.core.stream.StreamProcessor;

import java.util.List;
import java.util.function.Consumer;

@Slf4j
@Service
public class TinkoffApiService {

    @Autowired
    InvestApi investApi;

    public void connectToMarketDataStream(List<String> figiList, StreamProcessor<MarketDataResponse> processor) {

        // На вход подаётся поток запросов.
        var request = FlowAdapters.toFlowPublisher(
                Multi.createFrom().<MarketDataRequest>emitter(emitter -> {
                    emitter.emit(marketDataSubscribeTradesRequest(figiList));
                    // emitter.complete(); <-- Если остановить поток запросов, то остановиться и поток ответов.
                }));

        // На выходе поток ответов.
        Consumer<Throwable> onErrorCallback = error -> log.error(error.toString());
        investApi.getMarketDataStreamService().newStream("id", processor, onErrorCallback).subscribeTrades(figiList);

    }

    private static MarketDataRequest marketDataSubscribeTradesRequest(List<String> figiList) {
        var builder = SubscribeTradesRequest.newBuilder()
                .setSubscriptionAction(SubscriptionAction.SUBSCRIPTION_ACTION_SUBSCRIBE);
        for (String figi : figiList) {
            builder.addInstruments(TradeInstrument.newBuilder().setFigi(figi).build());
        }
        return MarketDataRequest.newBuilder()
                .setSubscribeTradesRequest(builder.build())
                .build();
    }

}
