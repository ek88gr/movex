package iisis.trade.movex.externalSource.tinkoff;

import iisis.trade.movex.externalSource.tinkoff.api.TinkoffApiService;
import iisis.trade.movex.pair.PairService;
import iisis.trade.movex.pair.models.MarketTrade;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import ru.tinkoff.piapi.contract.v1.*;

import java.time.Instant;
import java.util.Arrays;
import java.util.List;

@Slf4j
@Getter
@Setter
@Service
public class TinkoffService {

    @Autowired
    TinkoffApiService tinkoffApiService;

    @Autowired
    PairService pairService;

    public void init() {
        tinkoffApiService.connectToMarketDataStream(getFigiList(), item -> {
            if (item.hasTrade()) {
                try {
                    log.debug("Новые данные по сделкам: {}", item);
                    Trade v = item.getTrade();

                    final var t = v.getTime();

                    final MarketTrade marketTrade = new MarketTrade();
                    marketTrade.setCurrencyPair(this.getByFigi(v.getFigi()));
                    marketTrade.setTradeTime(Instant.ofEpochSecond(t.getSeconds(), t.getNanos()).toEpochMilli());
                    marketTrade.setQuantity(v.getQuantity());

                    var p = v.getPrice();
                    var sUnit = String.valueOf(p.getUnits());
                    var sNano = String.valueOf(p.getNano());
                    var price = Double.parseDouble(sUnit + "." + sNano);
                    marketTrade.setPrice(price);

                    pairService.putStream(Arrays.asList(marketTrade));

                } catch (Exception e) {
                    log.error("ERROR", e);
                }

            }
        });
    }


    public List<String> getFigiList() {
//        Сбербанк, Газпром, МТС, Детский мир, Северсталь, ВТБ, Яндекс, Полюс Золото
        return Arrays.asList(
                "BBG004730N88",
                "BBG004730RP0",
                "BBG004S681W1",
                "BBG000BN56Q9",
                "BBG00475K6C3",
                "BBG004730ZJ9",
                "BBG006L8G4H1",
                "BBG000R607Y3"
        );
    }

    private CurrencyPair getByFigi(String figi) {
        switch (figi) {
            case "BBG004730N88":
                return new CurrencyPair("SBER", "RUB");
            case "BBG004730RP0":
                return new CurrencyPair("GAZ", "RUB");
            case "BBG004S681W1":
                return new CurrencyPair("MTS", "RUB");
            case "BBG000BN56Q9":
                return new CurrencyPair("DMIR", "RUB");
            case "BBG00475K6C3":
                return new CurrencyPair("SSTAL", "RUB");
            case "BBG004730ZJ9":
                return new CurrencyPair("VTB", "RUB");
            case "BBG006L8G4H1":
                return new CurrencyPair("YAND", "RUB");
            case "BBG000R607Y3":
                return new CurrencyPair("PZOL", "RUB");
        }
        throw new RuntimeException("figi not found: " + figi);
    }

}
