package iisis.trade.movex.externalSource.tinkoff.api;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import ru.tinkoff.piapi.core.InvestApi;

@Slf4j
@Configuration
public class TinkoffApiConfig {
    @Value("${iisis.trade.movex.externalSource.tinkoff.api.TOKEN}")
    private String token;

    @Bean
    public InvestApi getInvestApi(){
        return InvestApi.create(token);
    }
}