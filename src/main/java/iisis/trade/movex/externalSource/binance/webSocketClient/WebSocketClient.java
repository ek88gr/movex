package iisis.trade.movex.externalSource.binance.webSocketClient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;
import reactor.core.publisher.Flux;

import java.util.List;
import java.util.Timer;
import java.util.TimerTask;
import java.util.concurrent.ConcurrentHashMap;

@Component
@Slf4j
public class WebSocketClient {

    private final String URL = "wss://stream.binance.com:9443";

    private ConcurrentHashMap<String, WebSocketConnection> CONNECTION_MAP = new ConcurrentHashMap<>();
    Timer PING_TIMER = new Timer("WebSocketClient.PING");

    //частота проверки задания (как часто высчитывать timeDifference)
    private final Long TIMER_PERIOD = 30 * 1000L;

    public WebSocketClient() {
        PING_TIMER.schedule(new TimerTask() {
            @Override
            public void run() {
                CONNECTION_MAP.forEach((k, v) -> v.ping());
            }
        }, 0, TIMER_PERIOD);
    }

    public Flux<String> connect(List<String> streams) {

        String url = URL + _getUrlPath(streams);

        var connection = CONNECTION_MAP.computeIfAbsent(url,
                key ->
                        new WebSocketConnection(key, 60 * 1000L, 3)
        );

        return connection.connect();
    }

    private String _getUrlPath(List channels) {
        StringBuilder url = new StringBuilder("/stream?streams=");
        url.append(channels.get(0));

        for (int i = 1; i < channels.size(); i++) {
            url.append("/").append(channels.get(i));
        }

        return url.toString();
    }

}
