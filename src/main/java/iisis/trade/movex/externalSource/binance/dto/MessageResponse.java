package iisis.trade.movex.externalSource.binance.dto;

import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
@Setter
public class MessageResponse <T> {
    final private String type;
    final private T data;

    public MessageResponse(String t, T data){
        this.type = t;
        this.data = data;
    }
}