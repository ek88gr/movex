package iisis.trade.movex.externalSource.binance;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import iisis.trade.movex.externalSource.binance.webSocketClient.WebSocketClient;
import iisis.trade.movex.pair.PairService;
import iisis.trade.movex.pair.models.MarketTrade;
import lombok.extern.slf4j.Slf4j;
import org.knowm.xchange.currency.CurrencyPair;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.Optional;

@Slf4j
@Service
public class BinanceService {

    final ObjectMapper mapper = new ObjectMapper();

    @Autowired
    WebSocketClient webSocketClient;

    @Autowired
    PairService pairService;


    public void init() {
        var pairs = Arrays.asList("usdtrub@trade", "btcrub@trade", "ethrub@trade", "busdrub@trade", "bnbrub@trade");
        webSocketClient
                .connect(pairs)
                .subscribe(v -> {
//                    log.info(v);
                    Optional<MarketTrade> data = getPairOrderByJson(v);
                    if (data.isPresent()) {
                        pairService.putStream(Arrays.asList(data.get()));
                    }
                });
    }


    private Optional<MarketTrade> getPairOrderByJson(String data) {
        final MarketTrade el = new MarketTrade();

        final JsonNode jsonNode;
        try {
            jsonNode = mapper.readTree(data);
            final var currencyPair = getCurrencyPairByStreamName(jsonNode.get("stream").asText());
            final var jsonNodeData = jsonNode.get("data");

            el.setCurrencyPair(currencyPair);
            el.setEventType(jsonNodeData.get("e").asText());
            el.setEventTime(jsonNodeData.get("E").asLong());
//        el.setCurrencyPair(jsonNode.get("s").asText());
            el.setPrice(Double.parseDouble(jsonNodeData.get("p").asText()));
            el.setQuantity(Double.parseDouble(jsonNodeData.get("q").asText()));
            el.setTradeTime(jsonNodeData.get("T").asLong());

        } catch (Exception e) {
            log.error("failure convert DepthSnapshot: {}", data, e);
            return Optional.empty();
        }


        return Optional.of(el);
    }

    public CurrencyPair getCurrencyPairByStreamName(String streamName) {
        switch (streamName) {
            case "usdtrub@trade":
                return new CurrencyPair("USDT", "RUB");
            case "btcrub@trade":
                return CurrencyPair.BTC_RUB;
            case "ethrub@trade":
                return new CurrencyPair("ETH", "RUB");
            case "busdrub@trade":
                return new CurrencyPair("BUSD", "RUB");
            case "bnbrub@trade":
                return new CurrencyPair("BNB", "RUB");

        }

        throw new RuntimeException("streamName not found: " + streamName);
    }
}
