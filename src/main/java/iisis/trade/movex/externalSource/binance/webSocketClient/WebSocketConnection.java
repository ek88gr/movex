package iisis.trade.movex.externalSource.binance.webSocketClient;

import lombok.extern.slf4j.Slf4j;
import org.springframework.web.reactive.socket.WebSocketMessage;
import org.springframework.web.reactive.socket.client.ReactorNettyWebSocketClient;
import org.springframework.web.reactive.socket.client.WebSocketClient;
import reactor.core.Disposable;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Sinks;
import reactor.util.concurrent.Queues;

import java.net.URI;
import java.time.Instant;
import java.util.concurrent.atomic.AtomicLong;


@Slf4j
public class WebSocketConnection {

    private final Sinks.Many<String> sink = Sinks.many().multicast().onBackpressureBuffer(Queues.SMALL_BUFFER_SIZE, false);
    private final Flux<String> STREAM;
    private final URI CONNECTION_URI;

    /*
    Период, в течение которого не поступают данные с сервера для срабатывания переподлюкчения к серверу.
    От слишком частых переподключений к серверу может сработать защита,
поэтому в каждом случае нужно учитывать этот момент через параметр CONST_LAST_PING_DOWN_MS
    */
    private long CONST_LAST_PING_DOWN_MS = 60 * 1000L;

    /*при сетевой ошибке, отключения подключения со стороны сервера переподключаемся CONST_NUM_CONNECTION_RETRIES раз.
    Не злоупотребляем колчиством подключений, потому как на стороне сервера может быть защита(блок) на частые подключения */
    private long CONST_CONNECTION_RETRIES = 3;

    private final AtomicLong LAST_PING_TIME = new AtomicLong(-1);

    private Disposable CONNECTION;

    public WebSocketConnection(String url, long lastPingDownMs, long connectionRetries) {
        CONST_LAST_PING_DOWN_MS = lastPingDownMs;
        CONST_CONNECTION_RETRIES = connectionRetries;

        CONNECTION_URI = URI.create(url);
        this.STREAM = sink
                .asFlux()
                .share();
    }

    //проверяет состяние подключения, при необходимости переподключается к серверу
    public void ping() {
        if (Instant.now().toEpochMilli() > LAST_PING_TIME.get() + CONST_LAST_PING_DOWN_MS) {
            log.error("PING function: Reopen by CONST_LAST_PING_DOWN_MS: {} url: {} Thread_name: {}", CONST_LAST_PING_DOWN_MS, CONNECTION_URI.toString(), Thread.currentThread().getName());
            this.reopen();
        } else {
            log.info("Connection is open.  websocket url: {} Thread_name: {}", CONNECTION_URI.toString(), Thread.currentThread().getName());
        }
    }

    public Flux<String> connect() {
        log.info("CONNECT");
        this.open();

        return STREAM;
    }

    public void reopen() {
        log.info("REOPEN");
        CONNECTION.dispose();
        this.open();
    }

    public void open() {
        log.info("OPEN");
        this.LAST_PING_TIME.set(Instant.now().toEpochMilli());

        WebSocketClient client = new ReactorNettyWebSocketClient();
        CONNECTION = client
                .execute(CONNECTION_URI, session -> {
                    return session.receive()
                            .map(WebSocketMessage::getPayloadAsText)
                            .doOnNext(v -> {
                                this.LAST_PING_TIME.set(Instant.now().toEpochMilli());
                                this.sink.tryEmitNext(v);
                            })
                            .then();
                })
                .doOnCancel(() -> {
                    log.info("****************************************** doOnCancel****************************************** ");
                })
                .doOnSuccess(v -> {
                    log.info("****************************************** doOnSuccess: v {} ****************************************** ", v);
                })
                .doOnError(e -> {
                    log.error("******************************************  doOnError ****************************************** ", e);
                })
                .retry(CONST_CONNECTION_RETRIES)
                .subscribe();
    }

    public void close() {
        log.info("CLOSE");
        CONNECTION.dispose();
    }

}
