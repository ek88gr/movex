package iisis.trade.movex.externalSource;

import iisis.trade.movex.pair.PairService;
import iisis.trade.movex.pair.models.MarketTrade;
import lombok.Getter;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Slf4j
@Getter
@Setter
@RestController
public class ExternalSourceRestController {

    @Autowired
    PairService pairService;

    @PostMapping(value = "/orders")
    public void getOrderList(@RequestBody List<MarketTrade> orderList) {
        pairService.putStream(orderList);
    }

}
