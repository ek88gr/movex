-- create table "pair_element"
-- (
--     id bigint not null,
--     "pair" varchar(255) not null,
--     "time_frame" varchar(255) not null,
--     "percent_price_limit" varchar(255) not null
-- );

INSERT INTO "public"."users" (id, chat_id) VALUES (1, '-750522799');
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('USDT/RUB', 1, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('USDT/RUB', 1, 10, 2);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('USDT/RUB', 1, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BTC/RUB', 1, 5, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BTC/RUB', 1, 10, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BTC/RUB', 1, 15, 4);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('ETH/RUB', 1, 5, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('ETH/RUB', 1, 10, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('ETH/RUB', 1, 15, 4);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BUSD/RUB', 1, 1, 0.1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BUSD/RUB', 1, 5, 0.1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BUSD/RUB', 1, 10, 0.1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BNB/RUB', 1, 1, 0.1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BNB/RUB', 1, 5, 0.1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BNB/RUB', 1, 10, 0.1);

INSERT INTO "public"."users" (id, chat_id) VALUES (2, '-750522799');
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SBER/RUB', 2, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SBER/RUB', 2, 10, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SBER/RUB', 2, 15, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('GAZ/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('GAZ/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('GAZ/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('MTS/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('MTS/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('MTS/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('DMIR/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('DMIR/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('DMIR/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SSTAL/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SSTAL/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('SSTAL/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('VTB/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('VTB/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('VTB/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('YAND/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('YAND/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('YAND/RUB', 2, 15, 3);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('PZOL/RUB', 2, 5, 1);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('PZOL/RUB', 2, 10, 2);
-- INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('PZOL/RUB', 2, 15, 3);


INSERT INTO "public"."users" (id, chat_id) VALUES (3, '774254499');
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BTC/RUB', 3, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BTC/RUB', 3, 15, 2);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('ETH/RUB', 3, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('ETH/RUB', 3, 15, 2);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BUSD/RUB', 3, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BUSD/RUB', 3, 15, 2);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BNB/RUB', 3, 5, 1);
INSERT INTO "public"."pair_element" (pair, user_id, time_frame, percent_price_limit) VALUES ('BNB/RUB', 3, 15, 2);

