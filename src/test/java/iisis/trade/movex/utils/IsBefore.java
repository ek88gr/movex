package iisis.trade.movex.utils;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;

import java.time.ZonedDateTime;

public class IsBefore {

    /**
     * ПРОБЛЕМА - РЕШЕНИЕ
     * Поведение isBefore с двумя одинаковыми датами. Ожидаем false, потому что даты равны.
     * В итоге получаем, что переменные через метод ZonedDateTime.now() инициализируются
     * по-очереди и дата, инициализированная первой, оказывается равньше той, которая была
     * проинициализирована второй. В итоге получаем метод isBefore возвращает true.
     */
    @Test
    void isBefore() {

        ZonedDateTime timestamp1 = ZonedDateTime.now();
        ZonedDateTime timestamp2 = ZonedDateTime.now();

        try {
            Assertions.assertFalse(timestamp1.isBefore(timestamp2));
        } catch (Exception e) {
            System.out.println("Ожидаем false, но получаем true.");
            System.out.println(e);
        }

        Assertions.assertFalse(timestamp2.isBefore(timestamp1));

    }

}
