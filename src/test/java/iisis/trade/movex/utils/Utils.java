package iisis.trade.movex.utils;

import org.ta4j.core.BaseBar;

import java.time.Duration;
import java.time.ZonedDateTime;

public class Utils {

    public BaseBar getRandomBar() {

        float openPrice;
        float highPrice;
        float lowPrice;
        float closePrice;
        float volume;

        openPrice = (float) (Math.random() * 900) + 100;
        highPrice = (float) ((int) (Math.random() * 900) + 100);
        lowPrice = (float) ((int) (Math.random() * 900) + 100);
        closePrice = (float) ((int) (Math.random() * 900) + 100);
        volume = (float) ((int) (Math.random() * 900) + 100);

        return new BaseBar(
                Duration.ofMinutes(1),
                ZonedDateTime.now(),
                openPrice,
                highPrice,
                lowPrice,
                closePrice,
                volume
        );
    }

}
