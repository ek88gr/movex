package iisis.trade.movex.utils;

import org.junit.jupiter.api.Test;
import org.ta4j.core.*;
import org.ta4j.core.num.DoubleNum;

import java.time.Duration;
import java.time.ZoneId;
import java.time.ZonedDateTime;
import java.util.ArrayList;
import java.util.List;

class UtilsTest {

    Utils utils = new Utils();

    @Test
    public void test_01() {

        List<Bar> bars = new ArrayList<>();
        ZonedDateTime beginTime = ZonedDateTime.of(2022, 6, 6, 0, 0, 0, 0, ZoneId.systemDefault());
        ZonedDateTime endTime = ZonedDateTime.of(2022, 6, 6, 0, 5, 0, 0, ZoneId.systemDefault());

        BaseBar bar_1 = new BaseBar(Duration.ofMinutes(5), endTime, DoubleNum::valueOf);
        bar_1.addTrade(3.0, 178.0, DoubleNum::valueOf);
        BaseBar bar_2 = new BaseBar(Duration.ofMinutes(5), endTime.plusMinutes(5), DoubleNum::valueOf);
        bar_2.addTrade(2.0, 187.0, DoubleNum::valueOf);

        bars.add(bar_1);
        bars.add(bar_2);

        BarSeries series_1 = new BaseBarSeriesBuilder().withName("series_1").withBars(bars).build();

        System.out.println(series_1.getFirstBar().getLowPrice());

    }

    @Test
    void getRandomBarTest() {
        System.out.println("\n"+ utils.getRandomBar() + "\n");
    }
}