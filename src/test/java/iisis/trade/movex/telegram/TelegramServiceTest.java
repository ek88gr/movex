package iisis.trade.movex.telegram;

import iisis.trade.movex.message.MessageService;
import org.junit.jupiter.api.BeforeAll;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestInstance;
import org.springframework.beans.factory.annotation.Autowired;
import org.ta4j.core.BaseBar;
import org.ta4j.core.BaseBarSeries;
import org.ta4j.core.num.DoubleNum;
import org.telegram.telegrambots.meta.TelegramBotsApi;
import org.telegram.telegrambots.meta.exceptions.TelegramApiException;
import org.telegram.telegrambots.updatesreceivers.DefaultBotSession;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashMap;


@TestInstance(TestInstance.Lifecycle.PER_CLASS)
class TelegramServiceTest {

    @Autowired
    private TelegramService telegramService;

    @Autowired
    private Bot bot;

    @BeforeAll
    void setUp() {

        try {
            this.telegramService = new TelegramService(/*510730730L, */"parsebarbot", "5372209451:AAFcgHOxYkvRTt4R2pWisBVFFa0oTqnlmvU");
        } catch (Exception e) {
            System.out.println(e.getMessage());
        }

    }

    MessageService messageService = new MessageService();
    BaseBarSeries series = new BaseBarSeries("seriesTest", DoubleNum::valueOf);


    @Test
    void sendMessage() {

        BaseBar bar1 = new BaseBar(Duration.ofMinutes(1), ZonedDateTime.now(), 105.42, 112.99,
                90.0, 111.42, 1337);
        BaseBar bar2 = new BaseBar(Duration.ofMinutes(1), ZonedDateTime.now().plusMinutes(1), 10, 100.00,
                104.01, 11.42, 177);

        series.addBar(bar1);
        series.addBar(bar2);

        HashMap<String, String> data = new HashMap<>();
        data.put("pair", "USDTRUB");
        data.put("tradePrice", "98.406804f");
        data.put("priceDifference", "0.8");
        data.put("percentPriceLimit", "0.5");
        data.put("timeFrame", "1");
        data.put("openPrice", "0.3");
        data.put("closePrice", "0.35");
        data.put("lowPrice", "0.25");
        data.put("highPrice", "0.23");

        this.telegramService.sendMessage("-750522799", messageService.messageBuilder(data));

    }


    @Test
    void testBot() {
        try {
            TelegramBotsApi botsApi = new TelegramBotsApi(DefaultBotSession.class);
            botsApi.registerBot(bot);

        } catch (TelegramApiException e) {
            e.printStackTrace();
        }

    }

}