package iisis.trade.movex.pair;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.ta4j.core.Bar;
import org.ta4j.core.BarSeries;
import org.ta4j.core.BaseBar;
import org.ta4j.core.BaseBarSeries;
import org.ta4j.core.num.DoubleNum;
import org.ta4j.core.num.Num;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.util.HashMap;

class PairAdapterTest {

    @Test
    void testComparePriceBar() {
        PairAdapter adapter = new PairAdapter(1L, "USDT/RUB", new HashMap<>(1, 0.1f));
        final Duration BASE_BAR_DURATION = Duration.ofMinutes(1);
        final ZonedDateTime endTime = ZonedDateTime.now();

//        1-й тест
        final BaseBarSeries series1 = new BaseBarSeries("series1", DoubleNum::valueOf);
        final Integer timeFrame1 = 1;

        System.out.println("timeFrame > series.getBarCount() then result is empty");
        var result1 = adapter.comparePriceBar(timeFrame1, series1);
        Assertions.assertTrue(result1.isEmpty());

//        2-й тест
        final BaseBarSeries series2 = new BaseBarSeries("series2", DoubleNum::valueOf);
        final Integer timeFrame2 = 1;

        final Bar firstBar = new BaseBar(BASE_BAR_DURATION, endTime, DoubleNum::valueOf);
        firstBar.addTrade(DoubleNum.valueOf(198.0), DoubleNum.valueOf(0.59));
        series2.addBar(firstBar);

        final Bar lastBar = new BaseBar(BASE_BAR_DURATION, endTime.plusMinutes(1), DoubleNum::valueOf);
        lastBar.addTrade(DoubleNum.valueOf(198.0), DoubleNum.valueOf(0.44));
        series2.addBar(lastBar);

        System.out.println("fBarLow.compareTo(lBarLow) > 0");
        var result2 = adapter.comparePriceBar(timeFrame2, series2);
//        if (result2.isPresent()) {
//            Assertions.assertTrue(result2.get().getPriceDifference() > 0)
//        }
        result2.ifPresent(priceDifference -> Assertions.assertTrue(priceDifference.getPriceDifference() > 0));

//        3-й тест
        final BaseBarSeries series3 = new BaseBarSeries("series3", DoubleNum::valueOf);
        final Integer timeFrame3 = 1;

        final Bar thirdBar = new BaseBar(BASE_BAR_DURATION, endTime, DoubleNum::valueOf);
        thirdBar.addTrade(DoubleNum.valueOf(198.0), DoubleNum.valueOf(0.33));
        series3.addBar(thirdBar);

        final Bar fourthBar = new BaseBar(BASE_BAR_DURATION, endTime.plusMinutes(1), DoubleNum::valueOf);
        fourthBar.addTrade(DoubleNum.valueOf(198.0), DoubleNum.valueOf(0.49));
        series3.addBar(fourthBar);

        System.out.println("fBarLow.compareTo(lBarLow) < 0");
        var result3 = adapter.comparePriceBar(timeFrame3, series3);
        result3.ifPresent(priceDifference -> Assertions.assertFalse(priceDifference.getPriceDifference() < 0));


    }

    /**
     * В BaseBarSeries нельзя добавить новый бар, endTime которого <= endTime LastBar в BaseBarSeries
     */
    @Test
    void checkBaseBarSeriesOnDoubleBar() {
        BaseBarSeries series = new BaseBarSeries("test", DoubleNum::valueOf);

        final BaseBar bar1 = new BaseBar(Duration.ofMinutes(1), ZonedDateTime.now(), DoubleNum::valueOf);
        series.addBar(bar1);

        try {
            series.addBar(bar1);
        } catch (Exception e) {
            System.out.println("Не добавляет бары, которые не совпадают по временным отрезкам");
            System.out.println(e);

        }

    }


    /**
     * Есть предположение, что когда достаем данные из бара,
     * он их окгругляет до сотых (обрезает данные).
     * Предположение не подтвердилось.
     */
    @Test
    void checkRound() {

        BaseBarSeries series = new BaseBarSeries("series", DoubleNum::valueOf);
        Duration duration = Duration.ofMinutes(1);
        ZonedDateTime timePeriod = ZonedDateTime.now();


        Num tradePrice1 = DoubleNum.valueOf(63.58345646329);
        Num volume1 = DoubleNum.valueOf(4.0);
        BaseBar bar1 = new BaseBar(duration, timePeriod, DoubleNum::valueOf);
        bar1.addTrade(volume1, tradePrice1); // нет DoubleNum::valueOf, т.к. данные уже Num
        series.addBar(bar1);
        System.out.println("tradePrice1 = " + tradePrice1 + "\n");


        Num tradePrice2 = DoubleNum.valueOf(71.58340000000); // округлит до 71.5834
        Num volume2 = DoubleNum.valueOf(2.0);
        BaseBar bar2 = new BaseBar(duration, timePeriod.plusMinutes(1), DoubleNum::valueOf);
        bar2.addTrade(volume2, tradePrice2);
        series.addBar(bar2);
        System.out.println("tradePrice2 = " + tradePrice2 + "\n");


        Num tradePrice3 = DoubleNum.valueOf(104.8673967);
        Num volume3 = DoubleNum.valueOf(7.0);
        BaseBar bar3 = new BaseBar(
                duration,
                timePeriod.plusMinutes(2),
                70.453459375935,
                96.843735363,
                45.56749649,
                76.60368036804,
                5.0
        );

        System.out.println(
                "bar3.getOpenPrice() = " + bar3.getOpenPrice() + ", \n" +
                        "bar3.getHighPrice() = " + bar3.getHighPrice() + ", \n" +
                        "bar3.getLowPrice() = " + bar3.getLowPrice() + ", \n" +
                        "bar3.getClosePrice() = " + bar3.getClosePrice() + ", \n"
        );

        bar3.addTrade(volume3, tradePrice3);
        series.addBar(bar3);
        System.out.println(
                "bar3.getOpenPrice() = " + bar3.getOpenPrice() + ", \n" +
                        "bar3.getHighPrice() = " + bar3.getHighPrice() + ", \n" +
                        "bar3.getLowPrice() = " + bar3.getLowPrice() + ", \n" +
                        "bar3.getClosePrice() = " + bar3.getClosePrice() + ", \n"
        );


        Num tradePrice4 = DoubleNum.valueOf(99.867396700000000000000000000000000001);
        Num volume4 = DoubleNum.valueOf(4.0);
        BaseBar bar4 = new BaseBar(duration, timePeriod.plusMinutes(3), DoubleNum::valueOf);

        System.out.println(bar4.getOpenPrice());

        bar4.addTrade(volume4, tradePrice4);
        series.addBar(bar4);
        System.out.println( // через бар
                "bar4.getOpenPrice() = " + bar4.getOpenPrice() + ", \n" +
                        "bar4.getHighPrice() = " + bar4.getHighPrice() + ", \n" +
                        "bar4.getLowPrice() = " + bar4.getLowPrice() + ", \n" +
                        "bar4.getClosePrice() = " + bar4.getClosePrice() + " \n"
        );
        System.out.println( // через сириез и lastBar
                "openPrice() = " + series.getLastBar().getOpenPrice() + ", \n" +
                        "highPrice() = " + series.getLastBar().getHighPrice() + ", \n" +
                        "lowPrice() = " + series.getLastBar().getLowPrice() + ", \n" +
                        "closePrice() = " + series.getLastBar().getClosePrice() + " \n"
        );


    }


    @Test
    void test() {

        BarSeries series = new BaseBarSeries("my_2022_series", DoubleNum::valueOf);

        Duration barDuration = Duration.ofMinutes(10);
        ZonedDateTime barEndTime = ZonedDateTime.now().plus(barDuration);

        BaseBar bar1 = new BaseBar(barDuration, barEndTime, 111.43, 112.83, 107.77, 107.99, 1234);
        BaseBar bar2 = new BaseBar(barDuration, barEndTime.plusMinutes(1), 120.90, 117.50, 107.90, 115.42, 4242);
        BaseBar bar3 = new BaseBar(barDuration, barEndTime.plusMinutes(2), 171.43, 112.83, 107.22, 107.14, 1299);

//        BaseBar bar = BaseBar.builder(DecimalNum::valueOf, Number.class)
//                .timePeriod(Duration.ofDays(1))
//                .endTime(endTime)
//                .openPrice(125.0)
//                .highPrice(124.04)
//                .lowPrice(112.06)
//                .closePrice(100.67)
//                .volume(1256.12)
//                .build();

        series.addBar(bar1);
        series.addBar(bar2);
        series.addBar(bar3);

        System.out.println("FirsBar: " + series.getFirstBar());
        System.out.println("LastBar: " + series.getLastBar());

    }


}
