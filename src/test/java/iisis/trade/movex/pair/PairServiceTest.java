package iisis.trade.movex.pair;

import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.Test;
import org.springframework.stereotype.Component;
import org.ta4j.core.*;
import org.ta4j.core.num.DoubleNum;
import org.ta4j.core.num.Num;

import java.time.Duration;
import java.time.ZonedDateTime;
import java.time.temporal.ChronoUnit;
import java.util.ArrayList;
import java.util.List;

@Component
class PairServiceTest {

    @Test
    void priceNotEqualLowOrHigh() {
        PairService pairService = new PairService();
        final Num price = DoubleNum.valueOf(58.6d);

//        1. price.isEqual(low)
        final Num low1 = price;
        final Num high1 = DoubleNum.valueOf(60.7d);
        Assertions.assertFalse(pairService.priceNotEqualLowOrHigh(price, low1, high1));

//        2. price.isEqual(high)
        final Num low2 = DoubleNum.valueOf(48.5d);
        final Num high2 = price;
        Assertions.assertFalse(pairService.priceNotEqualLowOrHigh(price, low2, high2));
    }


    @Test
    /**
     * Если сравнивать с массивом, то:
     * первый элемент массива - это FirstBar,
     * последний элемент массива - это LastBar.
     */
    void FirstOrLastBarV1(){
        final Duration BASE_BAR_DURATION = Duration.ofMinutes(1);
        final ZonedDateTime endTime = ZonedDateTime.now();

        List<BaseBar> list = new ArrayList();
        list.add(new BaseBar(BASE_BAR_DURATION, endTime, DoubleNum::valueOf));
        list.add(new BaseBar(BASE_BAR_DURATION, endTime.plus(BASE_BAR_DURATION), DoubleNum::valueOf));

        BaseBarSeries series = new BaseBarSeries("test", DoubleNum::valueOf);
        series.addBar(list.get(0));
        series.addBar(list.get(1));

        System.out.println("Первый элемент массива - это FirstBar");
        Assertions.assertEquals(series.getFirstBar(), list.get(0));
        System.out.println("Последний элемент массива - это LastBar");
        Assertions.assertEquals(series.getLastBar(), list.get(1));

        Assertions.assertTrue(series.getLastBar().equals(list.get(1)));

    }


    @Test
    /**
     * Путаем первый и последний бар в series. Создаем series, добавляем два бара, выводим
     * первый и последний. Первый бар в series - firstBar, последний бар в series - lastBar.
     * Обязательно по каждому бару добавить сделку addTrade, иначе бар не добавить в series.
     */
    void FirstOrLastBarV2(){
        final Duration BASE_BAR_DURATION = Duration.ofMinutes(1);
        final ZonedDateTime endTime = ZonedDateTime.now();

        BaseBarSeries series = new BaseBarSeries("test", DoubleNum::valueOf);

        BaseBar bar1 = new BaseBar(BASE_BAR_DURATION, endTime, DoubleNum::valueOf);
        bar1.addTrade(1000.0, 25, DoubleNum::valueOf);
        BaseBar bar2 = new BaseBar(BASE_BAR_DURATION, endTime.plus(BASE_BAR_DURATION), DoubleNum::valueOf);
        bar2.addTrade(2000.0, 50, DoubleNum::valueOf);

        series.addBar(bar1);
        series.addBar(bar2);

        System.out.println("\nПЕРВЫЙ БАР: " + series.getFirstBar() + "\n");
        System.out.println("ПОСЛЕДНИЙ БАР: " + series.getLastBar() + "\n");

    }


    @Test
    /**
     * Смотрим, как будет вести себя программа, если будет стоять ограничение , например, в 5 баров.
     * Последний бар остается неизменным, первый смещается на 5 элемент от первого бара и занимаем 5
     * позицию, нумерация баров с нуля.
     */
    void FirstOrLastBarV3(){
        final ZonedDateTime endTime = ZonedDateTime.now();

        BaseBarSeries testSeries = new BaseBarSeries("testSeries");
        testSeries.setMaximumBarCount(5);

        testSeries.addBar(endTime, 100.0, 112.83, 107.77, 107.99, 1234);
        testSeries.addBar(endTime.plusMinutes(1), 200.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(2), 300.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(3), 400.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(4), 500.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(5), 600.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(6), 700.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(7), 800.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(8), 900.0, 117.50, 107.90, 115.42, 4242);
        testSeries.addBar(endTime.plusMinutes(9), 1000.0, 117.50, 107.90, 115.42, 4242);
    }


    @Test
    void init() {

        BarSeries series = new BaseBarSeries("mySeries");
        series.setMaximumBarCount(20);

        Duration barDuration = Duration.ofMinutes(10);

        var barEndTime = ZonedDateTime.now().plus(barDuration);
        BaseBar baseBar = new BaseBar(barDuration, barEndTime, DoubleNum::valueOf);

        series.addBar(barEndTime.plusMinutes(1), 111.43, 112.83, 107.77, 107.99, 1234);
        series.addBar(barEndTime.plusMinutes(2), 120.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(3), 121.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(4), 107.00, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(5), 19.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(6), 109.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(7), 10.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(8), 108.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(9), 107.30, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(10), 106.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(11), 107.20, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(12), 106.70, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(13), 102.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(14), 101.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(15), 107.15, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(16), 104.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(17), 108.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(18), 199.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(19), 22.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(20), 27.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(21), 32.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(22), 10.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(23), 7.90, 117.50, 107.90, 115.42, 4242);
        series.addBar(barEndTime.plusMinutes(24), 5.90, 117.50, 107.90, 115.42, 4242);


        System.out.println(series.getBar(0)); // 1-й
        System.out.println(series.getBar(20)); // 21-й
        System.out.println(series.getBar(23) + "\n"); // 24-й
//        System.out.println(series.getBar(24)); // 25-й (ошибка)
        System.out.println(series.getFirstBar());
        System.out.println(series.getLastBar());


        BaseBar bar2 = new BaseBar(Duration.ofMinutes(5), ZonedDateTime.now().plusMinutes(5), 105.42, 112.99,
                104.01, 111.42, 1337);

        var tradeTimeStamp = ZonedDateTime.now().plus(5, ChronoUnit.SECONDS);

//        ZonedDateTime tradeTimeStamp = ZonedDateTime.ofInstant(Instant.ofEpochMilli(Long.parseLong(tradeLine[0]) * 1000), ZoneId.systemDefault());


        int i = 0;
        while (baseBar.inPeriod(tradeTimeStamp)) {
            final Num tradeVolume = DoubleNum.valueOf(10 + i);
            final Num tradePrice = DoubleNum.valueOf(150 + i);
            baseBar.addTrade(tradeVolume, tradePrice);
            if (i > 30) break;
            i++;
        }

    }


    private boolean inPeriod(ZonedDateTime timestamp) {
        ZonedDateTime beginTime = ZonedDateTime.now();
        ZonedDateTime endTime = ZonedDateTime.now().plusMinutes(1);
        boolean result;
        if (timestamp != null && !timestamp.isBefore(beginTime) && timestamp.isBefore(endTime)) {
            result = true;
        } else {
            result = false;
        }
        return result;
    }

    @Test
    void inPeriodTest() {
        inPeriod(ZonedDateTime.now());
    }

}

















